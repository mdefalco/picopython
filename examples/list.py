l1 = [ 1, 2, 3 ]
l2 = [ k+2 for k in l1 ]
l3 = [ k for k in range(12) ]
print(l1, l2, l3)
print(l3[0])
print(l3[-1])
print(l3[1:3])
print(l3[:3])
print(l3[1:])
print(l3[:])
