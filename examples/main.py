# ceci est un commentaire séparé
3 # ceci est un commentaire en fin de ligne

# fonction print
print()

# affectation
a = 1
print(a)
a, b  = 1, 2
print(a, b)
a, b = b, a
print(a, b)

# manipulation d'entiers
print(1, -3, +2)
print(1+2,-1+2,1-2)
print(1*2, 3/2, 3//2)
print(3**2)

# manipulation de flottants
print(1.3, 0.2, .2, 1.)
print(1.+2.,1.+2,1+2.)
print(1.*2.,1.*2,1*2.)
print(1./2.,1./2,1/2.)
print(3.**2., 3.**0.5, 3.**-1)

# boucle
for i in range(3):
    print(i)

l = [1,2]
for e in l:
    print(e)

for e in l: print(e)

for e in [1,2]:
    print(e)

for a, b in [ (1,2) ]:
    print(a,b)

i = 0
while i < 3:
    i = i + 1
    print(i)

# listes
print([], [1], [1,2])
l = [0,1,2,3]
print(l[0],l[1:],l[:3],l[1:3], l[:])
l[0] = 1
l[-1] = 5
print(l)
print(list(range(3)))
print(list( (1,2) ))
print([1,2,3] + [4,5])
l = [1,2]
l += [3]
print(l)

# tuples
print((), (1,), (1,2))
l = (0,1,2,3)
print(l[0],l[1:],l[:3],l[1:3], l[:])

# chaine de caractères
print('toto')
print(type(3))

# booleens
print(1 and 0, 1 and 1, 1 or 0, not 1, not 0)
print([] or (1,2))

# virgule optionnelle de fin d'expressions
print(1,)
print( (1,) ) # necessaire pour créer un tuple
print( [1,] )

# augment assign
x = 1
x += 2
print(x)
x *= 2
print(x)
x -= 3
print(x)

# string
s = 'abcd'
print(s)
print(s[0], s[1:3], s[-1], s[2:])
for c in s:
    print(c)
print('abcd' + 'efg')

# comparison of iterables
print('abcd' < 'abc')
print('abcd' == 'abc')
print('abcd' == 'abcd')
print([1,3] < [1])
print([1,2,3] >= [1,2,3])
print([0,1,2] == list(range(3)))
print([ [0,1], [2,3] ] < [ [0,1], [2,4] ])
