for a, b in [ (1,2) ]:
    print(a,b)

3

print()

# affectation
a = 1
print(a)
a, b  = 1, 2
print(a, b)
a, b = b, a
print(a, b)

print(1)
print(1+2,-1+2,1-2)
print(1*2, 3/2, 3//2)

# boucle
for i in range(3):
    print(i)

l = [1,2]
for e in l:
    print(e)


for e in [1,2]:
    print(e)


for e in l: print(e)

i = 0
while i < 3:
    i = i + 1
    print(i)
