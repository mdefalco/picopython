//
// Created by marc on 04/11/18.
//

#ifndef PICOPYTHON_LEXER_H
#define PICOPYTHON_LEXER_H

typedef struct LexerState {
    unsigned int pos;
    unsigned int depth;
    char *buf;
} LexerState;

typedef enum {
    TOKEN_DEPTH_INCREASE,
    TOKEN_DEPTH_DECREASE,
    TOKEN_OPERATOR,
    TOKEN_NEWLINE,
    TOKEN_COLON,
    TOKEN_COMMA,
    TOKEN_LPAREN,
    TOKEN_RPAREN,
    TOKEN_LBRACKET,
    TOKEN_RBRACKET,
    TOKEN_DOT,
    TOKEN_IDENT,
    TOKEN_ASSIGN,
    TOKEN_PLUS,
    TOKEN_EQUALS,
    TOKEN_MINUS,
    TOKEN_TIMES,
    TOKEN_DIVIDE,
    TOKEN_TRUEDIVIDE,
    TOKEN_INTEGER,
    TOKEN_FLOAT,
    TOKEN_STRING,
    TOKEN_SEMICOLON,
    TOKEN_KEYWORD,
    TOKEN_LESS,
    TOKEN_DIFF,
    TOKEN_MODULO,
    TOKEN_GREATER,
    TOKEN_LESS_EQUAL,
    TOKEN_GREATER_EQUAL,
    TOKEN_POWER,
    TOKEN_BANG,
    TOKEN_AUG_TIMES,
    TOKEN_AUG_MINUS,
    TOKEN_AUG_PLUS,
    TOKEN_EOF
} TokenType;

typedef enum {
    KEYWORD_TRUE,
    KEYWORD_FALSE,
    KEYWORD_FOR,
    KEYWORD_WHILE,
    KEYWORD_IF,
    KEYWORD_IN,
    KEYWORD_ELSE,
    KEYWORD_DEF,
    KEYWORD_NOT,
    KEYWORD_OR,
    KEYWORD_AND
} Keyword;

typedef struct Token {
    TokenType type;
    union {
        char *text;
        int integer;
        double floating;
        Keyword keyword;
    };
    unsigned int pos;
    unsigned int line;
    unsigned int start;
    unsigned int end;
} Token;

typedef struct Tokens {
    Token *tokens;
    unsigned int numTokens;
    unsigned int arraySize;
} Tokens;

Tokens stringToTokens(char *buf);
void pprint_tokens(Tokens tokens);
const char *sprint_tokenType(TokenType tokenType);

#endif //PICOPYTHON_LEXER_H
