//
// Created by marc on 08/11/18.
//

#ifndef PICOPYTHON_HASHTABLE_H
#define PICOPYTHON_HASHTABLE_H

#include <stdbool.h>

#define HASHTABLE_SIZE 512
typedef struct HashTableListNode {
    unsigned char *key;
    void *value;
    struct HashTableListNode *next;
} HashTableList;

typedef HashTableList** HashTable;

HashTable hashTableInit();
void hashTableInsert(HashTable h, unsigned char *key, void *value);
void *hashTableUpdate(HashTable h, unsigned char *key, void *value);
bool hashTableTest(HashTable h, unsigned char *key);
void *hashTableGet(HashTable h, unsigned char *key);

#endif //PICOPYTHON_HASHTABLE_H
