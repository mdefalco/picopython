//
// Created by marc on 08/11/18.
//

#ifndef PICOPYTHON_EVALUATOR_H
#define PICOPYTHON_EVALUATOR_H

#include "ast.h"
#include "hashtable.h"

typedef enum {
    VALUE_NONE,
    VALUE_INTEGER,
    VALUE_FLOAT,
    VALUE_RANGE,
    VALUE_BUILTIN,
    VALUE_TUPLE,
    VALUE_LIST,
    VALUE_FUNCTION,
    VALUE_STRING,
    VALUE_OBJECT
} PythonType;

struct builtins_range {
    int start;
    int stop;
    int step;
    int current;
};

typedef struct PythonValue PythonValue;
typedef struct PythonObject PythonObject;

typedef struct {
    PythonValue *elements;
    unsigned int length;
    unsigned int current;
} PythonValueSequence;

typedef struct {
    char *elements;
    unsigned int length;
    unsigned int current;
} PythonValueString;

typedef struct PythonValue {
    PythonType type;
    union {
        int integer;
        float floating;
        PythonValueSequence *seq;
        PythonValueString *string;
        struct builtins_range *rangeData;
        void (*builtin_function)(PythonValue,PythonValue*);
    };
} PythonValue;

typedef struct Environment {
    HashTable globals;
    HashTable locals;
    HashTable builtins;
} Environment;

typedef struct PythonObject {
    struct PythonObject *class;
} PythonObject;

PythonValue evaluate(Environment env, SyntaxTree *ast);
void pprint_env(Environment env);
Environment initMainEnvironment();
void pprint_value(PythonValue v);

#endif //PICOPYTHON_EVALUATOR_H
