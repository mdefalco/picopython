//
// Created by marc on 04/11/18.
//

#ifndef PICOPYTHON_AST_H
#define PICOPYTHON_AST_H

typedef enum SyntaxType {
    SYNTAX_FOREST,
    SYNTAX_TUPLE,
    SYNTAX_LIST,
    SYNTAX_DICT,
    SYNTAX_COMPREHENSION,
    SYNTAX_STATEMENTS,
    SYNTAX_ASSIGN,
    SYNTAX_STATEMENT_FOR,
    SYNTAX_OPERATION_BINARY,
    SYNTAX_OPERATION_UNARY,
    SYNTAX_IDENTIFIER,
    SYNTAX_CALL,
    SYNTAX_FOR,
    SYNTAX_WHILE,
    SYNTAX_INLINE_IF,
    SYNTAX_SLICE,
    SYNTAX_GETITEM,
    SYNTAX_GETATTR,
    SYNTAX_AUGMENT,
    SYNTAX_CONSTANT_INTEGER,
    SYNTAX_CONSTANT_FLOAT,
    SYNTAX_CONSTANT_STRING
} SyntaxType;

typedef enum SyntaxOperator {
    SOPE_PLUS,
    SOPE_MINUS,
    SOPE_OR,
    SOPE_AND,
    SOPE_NOT,
    SOPE_TIMES,
    SOPE_MODULO,
    SOPE_DIVIDE,
    SOPE_TRUEDIVIDE,
    SOPE_POWER,
    SOPE_GREATER_EQUAL,
    SOPE_LESS_EQUAL,
    SOPE_GREATER,
    SOPE_LESS,
    SOPE_DIFF,
    SOPE_EQUALS
} SyntaxOperator;

typedef struct SyntaxNode {
    SyntaxType type;
    union {
        unsigned char *text;
        int integer;
        double floating;
        SyntaxOperator operator;
    };
    struct SyntaxNode *arg;
    struct SyntaxNode *arg2;
    struct SyntaxNode *arg3;
    unsigned int line;
} SyntaxTree;

void pprint_tree(SyntaxTree *);
void pprint_expression(SyntaxTree *);

char *repr_tree(SyntaxTree *st);
void freeSyntaxTree(SyntaxTree *st);

#endif //PICOPYTHON_AST_H
