//
// Created by marc on 04/11/18.
//

#ifndef PICOPYTHON_PARSER_H
#define PICOPYTHON_PARSER_H

#include "lexer.h"
#include "ast.h"
#include <stdbool.h>

typedef struct ParserState {
    unsigned int pos;
    Tokens *tk;
    const char *code;
    char *errorDescription;
    bool error;
} ParserState;

ParserState initParser(const char *code, Tokens *tok);
SyntaxTree *parseArgumentsCall(ParserState *ps);
SyntaxTree *parseExpression(ParserState *ps);
SyntaxTree *parseStatement(ParserState *ps);
SyntaxTree *parseInput(ParserState *ps);

#endif //PICOPYTHON_PARSER_H
