# PicoPython

A lightweight C library to interpret a subdialect of Python. The purpose is to have a convenient alternative to liblua to include Python as a scripting language. Compilation to JavaScript via Emscripten is one of the main goal.