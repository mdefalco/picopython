//
// Created by marc on 04/11/18.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "ast.h"

bool isTerminalExpression(SyntaxTree *st)
{
    return st->type == SYNTAX_CONSTANT_INTEGER || st->type == SYNTAX_CONSTANT_FLOAT || st->type == SYNTAX_IDENTIFIER;
}

void pprint_terminalExpression(SyntaxTree *st)
{
    if (st->type == SYNTAX_IDENTIFIER)
        printf("%s", st->text);
    else if (st->type == SYNTAX_CONSTANT_INTEGER)
        printf("%d", st->integer);
    else if (st->type == SYNTAX_CONSTANT_FLOAT)
        printf("%f", st->floating);
}

void pprint_operand(SyntaxTree *st)
{
    if (isTerminalExpression(st)) {
        pprint_terminalExpression(st);
    } else {
        printf("(");
        pprint_expression(st);
        printf(")");
    }
}

void pprint_operator(SyntaxOperator ope)
{
    switch(ope) {
        case SOPE_PLUS: printf("+"); break;
        case SOPE_MINUS: printf("-"); break;
        case SOPE_EQUALS: printf("=="); break;
        case SOPE_TIMES: printf("*"); break;
        case SOPE_DIVIDE: printf("//"); break;
        case SOPE_MODULO: printf("%%"); break;
        case SOPE_TRUEDIVIDE: printf("/"); break;
        default: break;
    }
}

void pprint_arguments_call(SyntaxTree *st)
{
    pprint_expression(st->arg);
    if (st->arg2 != NULL) {
        printf(",");
        pprint_arguments_call(st->arg2);
    }
}

void pprint_expression(SyntaxTree *st)
{
    if (st->type == SYNTAX_OPERATION_BINARY)
    {
        pprint_operand(st->arg);
        pprint_operator(st->operator);
        pprint_operand(st->arg2);
    } else if (st->type == SYNTAX_OPERATION_UNARY) {
        pprint_operator(st->operator);
        pprint_operand(st->arg);
    } else if (st->type == SYNTAX_CALL) {
        pprint_operand(st->arg);
        printf("(");
        pprint_arguments_call(st->arg2);
        printf(")");
    } else
        pprint_terminalExpression(st);
}

void pprint_tree(SyntaxTree *st)
{
    switch(st->type)
    {
        case SYNTAX_STATEMENTS:
            pprint_tree(st->arg);
            printf("\n");
            if (st->arg2 != NULL) {
                pprint_tree(st->arg2);
                printf("\n");
            }
            break;
        case SYNTAX_FOR:
            printf("for ");
            pprint_tree(st->arg);
            printf(" in ");
            pprint_tree(st->arg2);
            printf(":");
            pprint_tree(st->arg3);
            printf("\n");
        case SYNTAX_ASSIGN:
            pprint_tree(st->arg);
            printf("=");
            pprint_tree(st->arg2);
            break;
        case SYNTAX_OPERATION_BINARY:
            pprint_tree(st->arg);
            pprint_operator(st->operator);
            pprint_tree(st->arg2);
            break;
        case SYNTAX_OPERATION_UNARY:
            pprint_operator(st->operator);
            pprint_tree(st->arg);
            break;
        case SYNTAX_CONSTANT_INTEGER:
            printf("%d", st->integer);
            break;
        case SYNTAX_CONSTANT_FLOAT:
            printf("%f", st->floating);
            break;
        case SYNTAX_IDENTIFIER:
            printf("%s", st->text);
            break;
        case SYNTAX_TUPLE:
            pprint_tree(st->arg);
            printf(", ");
            pprint_tree(st->arg2);
            break;
        case SYNTAX_AUGMENT:
            pprint_tree(st->arg);
            pprint_operator(st->operator);
            printf("=");
            pprint_tree(st->arg2);
        default:
            break;
    }
}

char *repr_tree(SyntaxTree *st)
{
    if (st == NULL) {
        printf("NULL\n");
        return "";
    }
    switch(st->type) {
        case SYNTAX_OPERATION_BINARY:
            printf("<%d|", (int)st->operator);
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(">");
            break;
        case SYNTAX_FOR:
            printf("<for|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(",");
            repr_tree(st->arg3);
            printf(">");
            break;
        case SYNTAX_CONSTANT_FLOAT:
            printf("%f", st->floating);
            break;
        case SYNTAX_CONSTANT_INTEGER:
            printf("%d", st->integer);
            break;
        case SYNTAX_IDENTIFIER:
            printf("%s", st->text);
            break;
        case SYNTAX_ASSIGN:
            printf("<ass|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(">");
            break;
        case SYNTAX_CALL:
            printf("<call|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(">");
            break;
        case SYNTAX_LIST:
            printf("<[]|");
            repr_tree(st->arg);
            printf(">");
            break;
        case SYNTAX_SLICE:
            printf("<slice|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(",");
            repr_tree(st->arg3);
            printf(">");
            break;
        case SYNTAX_GETITEM:
            printf("<");
            repr_tree(st->arg);
            printf("[");
            repr_tree(st->arg2);
            printf("]>");
            break;
        case SYNTAX_COMPREHENSION:
            printf("<comp|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(",");
            repr_tree(st->arg3);
            printf(">");
        case SYNTAX_TUPLE:
            printf("<()|");
            repr_tree(st->arg);
            printf(">");
            break;
        case SYNTAX_FOREST:
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            break;
        case SYNTAX_STATEMENTS:
            printf("<;|");
            repr_tree(st->arg);
            printf(",");
            repr_tree(st->arg2);
            printf(">");
            break;
        default:
            printf("<%d!!>", st->type);
    }
}

void freeSyntaxTree(SyntaxTree *st) {
    if (st != NULL) {
        if (st->arg != NULL) {
            freeSyntaxTree(st->arg);
        }
        if (st->arg2 != NULL) {
            freeSyntaxTree(st->arg2);
        }
        if (st->arg3 != NULL)
            freeSyntaxTree(st->arg3);
        if (st->type == SYNTAX_IDENTIFIER)
            free(st->text);
    }
    free(st);
}


