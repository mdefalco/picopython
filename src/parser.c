//
// Created by marc on 04/11/18.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "parser.h"

#define CHECK_ERROR() if(ps->error) return NULL;

SyntaxTree *parseFactor(ParserState *ps);
SyntaxTree *parseTestList(ParserState *ps);
SyntaxTree *parseTestListComp(ParserState *ps);
SyntaxTree *parseArgList(ParserState *ps);
SyntaxTree *parseSubscriptList(ParserState *ps);
Token *peekToken(ParserState *ps);

#define NODE_LIST_CODE(elementname) SyntaxTree *st = parse##elementname(ps); \
    if (ps->error) { return NULL; } \
    SyntaxTree *currentNode = NULL; \
    while (peekType(ps) == TOKEN_COMMA) { \
            nextToken(ps); \
        if (currentNode == NULL) { \
            SyntaxTree *arg = st; \
            st = allocSyntaxTree(ps); \
            st->type = SYNTAX_FOREST; \
            st->arg = arg; \
            st->arg2 = parse##elementname(ps); \
            currentNode = st; \
        } else { \
            SyntaxTree *parentNode = currentNode; \
            currentNode = allocSyntaxTree(ps); \
            currentNode->type = SYNTAX_FOREST; \
            currentNode->arg = parentNode->arg2; \
            currentNode->arg2 = parse##elementname(ps); \
            parentNode->arg2 = currentNode; \
        } \
    } \
    return st

SyntaxTree *tryParse(ParserState *ps, SyntaxTree *(*parseNode)(ParserState*))
{
        SyntaxTree *st = NULL;
        unsigned int savedPos = ps->pos;

        st = parseNode(ps);

        if (st == NULL) {
            ps->error = false;
            ps->pos = savedPos;
        }

        return st;
}

// Macro to parse list of items with an optional extra comma at the end
#define NODE_LIST_CODE_EX(elementname) SyntaxTree *st = parse##elementname(ps); \
    if (ps->error) { return NULL; } \
    SyntaxTree *currentNode = NULL; \
    while (peekType(ps) == TOKEN_COMMA) { \
        nextToken(ps); \
        SyntaxTree *elt = tryParse(ps, parse##elementname); \
        if (currentNode == NULL) { \
            SyntaxTree *arg = st; \
            st = allocSyntaxTree(ps); \
            st->type = SYNTAX_FOREST; \
            st->arg = arg; \
            st->arg2 = elt; \
            currentNode = st; \
        } else { \
            SyntaxTree *parentNode = currentNode; \
            currentNode = allocSyntaxTree(ps); \
            currentNode->type = SYNTAX_FOREST; \
            currentNode->arg = parentNode->arg2; \
            currentNode->arg2 = elt; \
            parentNode->arg2 = currentNode; \
        } \
        if (elt == NULL) break; \
    } \
    return st

#define PARSING_ERROR(a,b,c,d) parsingError(__LINE__,a,b,c,d)

SyntaxTree *allocSyntaxTree(ParserState *ps)
{
    SyntaxTree * st = (SyntaxTree *)malloc(sizeof(SyntaxTree));
    st->arg = st->arg2 = st->arg3 = NULL;
    if (peekToken(ps) != NULL)
        st->line = peekToken(ps)->line;
    else
        st->line = -1; // Wrong token
    return st;
}

ParserState initParser(const char *code, Tokens *tok)
{
   ParserState st;
   st.code = code;
   st.tk = tok;
   st.pos = 0;
   st.error = false;
   return st;
}

Token *peekToken(ParserState *ps)
{
    if (ps->pos >= ps->tk->numTokens) return NULL;
    return &(ps->tk->tokens[ps->pos]);
}


Token *peekNextToken(ParserState *ps)
{
    if (ps->pos+1 >= ps->tk->numTokens) return NULL;
    return &(ps->tk->tokens[ps->pos+1]);
}

TokenType peekType(ParserState *ps)
{
    if (peekToken(ps)) return peekToken(ps)->type;
    return TOKEN_EOF;
}


TokenType peekNextType(ParserState *ps)
{
    if (peekNextToken(ps)) return peekNextToken(ps)->type;
    return TOKEN_EOF;
}

void nextToken(ParserState *ps)
{
    ps->pos++;
}

bool isAugmentingToken(TokenType t)
{
    return t == TOKEN_AUG_TIMES | t == TOKEN_AUG_PLUS | t == TOKEN_AUG_MINUS;
}

bool isUnaryOperator(TokenType t)
{
    return t == TOKEN_MINUS;
}

bool isTermOperator(TokenType t)
{
    return t == TOKEN_TIMES | t == TOKEN_DIVIDE | t == TOKEN_TRUEDIVIDE | t == TOKEN_MODULO;
}

bool isComparisonOperator(TokenType t)
{
    return t == TOKEN_DIFF |  t == TOKEN_EQUALS | t == TOKEN_LESS
        | t == TOKEN_GREATER | t == TOKEN_LESS_EQUAL
        | t == TOKEN_GREATER_EQUAL;
}

SyntaxOperator convertTokenTypeToOperator(TokenType t)
{
    switch(t) {
        case TOKEN_AUG_MINUS: return SOPE_MINUS;
        case TOKEN_AUG_PLUS: return SOPE_PLUS;
        case TOKEN_AUG_TIMES: return SOPE_TIMES;
        case TOKEN_MINUS: return SOPE_MINUS;
        case TOKEN_PLUS: return SOPE_PLUS;
        case TOKEN_TIMES: return SOPE_TIMES;
        case TOKEN_DIVIDE: return SOPE_DIVIDE;
        case TOKEN_TRUEDIVIDE: return SOPE_TRUEDIVIDE;
        case TOKEN_EQUALS: return SOPE_EQUALS;
        case TOKEN_LESS_EQUAL: return SOPE_LESS_EQUAL;
        case TOKEN_GREATER_EQUAL: return SOPE_GREATER_EQUAL;
        case TOKEN_DIFF: return SOPE_DIFF;
        case TOKEN_LESS: return SOPE_LESS;
        case TOKEN_GREATER: return SOPE_GREATER;
        default: break;
    }
    /* FIXME shouldn't get there */
    return -1;
}

#define ERROR_BUFFER_SIZE 1024

void parsingError(unsigned int parserLine, ParserState *ps, Token *tok,
        const char *expected, const char *description)
{
    char *error = (char *) malloc(sizeof(char) * ERROR_BUFFER_SIZE);

    error[0] = '\0';

    unsigned int sz = ERROR_BUFFER_SIZE;
    unsigned int dec = 0;
    dec = snprintf(error, sz, "[Erreur de syntaxe (parser.c:%d)]\n", parserLine);
    int l = 0;
    int i = 0;
    int n = strlen(ps->code);
    if (tok != NULL) {
        dec += snprintf(error+dec, sz-dec,
                "Ligne %d, caractère %d:\n", tok->line, tok->start);
        while (l < tok->line) {
            if (ps->code[i++] == '\n') l++;
        }
        while(i < n && ps->code[i] != '\n') {
            dec += sprintf(error+dec, "%c", ps->code[i++]);
        }
        dec += sprintf(error+dec, "\n");
        for (int k=0; k < tok->start; k++) {
            dec += sprintf(error+dec, " ");
        }
        for (int k=tok->start; k < tok->end; k++) {
            dec += sprintf(error+dec, "^");
        }
        dec += sprintf(error+dec, "\n");
    }

    dec += snprintf(error+dec, sz-dec, "%s", description);

    ps->error = true;
    ps->errorDescription = error;
}

SyntaxTree *parseAtom(ParserState *ps)
{
    SyntaxTree *st = NULL;
    TokenType t = peekType(ps);

    switch(t) {
        case TOKEN_IDENT:
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_IDENTIFIER;
            st->text = peekToken(ps)->text;
            nextToken(ps);
            return st;
        case TOKEN_STRING:
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_CONSTANT_STRING;
            st->text = peekToken(ps)->text;
            nextToken(ps);
            return st;
        case TOKEN_INTEGER:
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_CONSTANT_INTEGER;
            st->integer = peekToken(ps)->integer;
            nextToken(ps);
            return st;
        case TOKEN_FLOAT:
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_CONSTANT_FLOAT;
            st->floating = peekToken(ps)->floating;
            nextToken(ps);
            return st;
        case TOKEN_LBRACKET: {
            nextToken(ps);
            if (peekType(ps) == TOKEN_RBRACKET) {
                nextToken(ps);
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_LIST;
                st->arg = NULL;
                return st;
            }
            st = parseTestListComp(ps);
            if (peekType(ps) != TOKEN_RBRACKET) {
                freeSyntaxTree(st);
                PARSING_ERROR(ps, peekToken(ps), "]", "");
                return NULL;
            }
            SyntaxTree *arg = st;
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_LIST;
            st->arg = arg;
            nextToken(ps);
            break; }
        case TOKEN_LPAREN: {
            nextToken(ps);
            if (peekType(ps) == TOKEN_RPAREN) {
                nextToken(ps);
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_TUPLE;
                st->arg = NULL;
                return st;
            }
            st = parseTestListComp(ps);
            if (peekType(ps) != TOKEN_RPAREN) {
                freeSyntaxTree(st);
                PARSING_ERROR(ps, peekToken(ps), ")", "");
                return NULL;
            }
            nextToken(ps);
            if (st->type == SYNTAX_FOREST) {
                // it's not a protected expression, it's a tuple
                SyntaxTree *arg = st;
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_TUPLE;
                st->arg = arg;
            }
            break; }
        case TOKEN_KEYWORD: {
            switch(peekToken(ps)->keyword) {
                case KEYWORD_TRUE:
                    st = allocSyntaxTree(ps);
                    st->type = SYNTAX_CONSTANT_INTEGER;
                    st->integer = 1;
                    break;
                case KEYWORD_FALSE:
                    st = allocSyntaxTree(ps);
                    st->type = SYNTAX_CONSTANT_INTEGER;
                    st->integer = 0;
                    break;
            }
            break;
        default:
            PARSING_ERROR(ps, peekToken(ps), "",
                    "Une expression atomique était attendu ici");
            break;
        }
    }

    return st;
}

SyntaxTree *parseAtomExpr(ParserState *ps)
{
    SyntaxTree *st = parseAtom(ps);

    while (peekType(ps) == TOKEN_DOT
        || peekType(ps) == TOKEN_LBRACKET
        || peekType(ps) == TOKEN_LPAREN) {
        SyntaxTree *arg = st;

        switch(peekType(ps)) {
            case TOKEN_DOT:
                nextToken(ps);
                if (peekType(ps) != TOKEN_IDENT) {
                    PARSING_ERROR(ps, peekToken(ps),
                            "un identifieur",
                            "On ne peut mettre qu'un indentifier directement après . pour accèder à un attribut.");
                    return NULL;
                }
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_GETATTR;
                st->arg = arg;
                st->text = peekToken(ps)->text;
                break;
            case TOKEN_LPAREN:
                nextToken(ps);
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_CALL;
                st->arg = arg;
                if (peekType(ps) == TOKEN_RPAREN) {
                    nextToken(ps);
                    st->arg2 = NULL;
                    return st;
                }
                st->arg2 = parseArgList(ps);
                CHECK_ERROR();
                if (peekType(ps) != TOKEN_RPAREN) {
                    PARSING_ERROR(ps, peekToken(ps), ")", "");
                    return NULL;
                }
                nextToken(ps);
                break;
            case TOKEN_LBRACKET:
                nextToken(ps);
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_GETITEM;
                st->arg = arg;
                st->arg2 = parseSubscriptList(ps);
                if (peekType(ps) != TOKEN_RBRACKET) {
                    PARSING_ERROR(ps, peekToken(ps), "]", "Un crochet fermant ']' était attendu ici");
                    return NULL;
                }
                nextToken(ps);
                break;
            default: {
                freeSyntaxTree(st);
                PARSING_ERROR(ps, peekToken(ps), ". [ ou (", "");
                return NULL;
            }
        }
    }

    return st;
}


SyntaxTree *parsePower(ParserState *ps)
{
    SyntaxTree *st = parseAtomExpr(ps);

    CHECK_ERROR();

    if (peekType(ps) == TOKEN_POWER) {
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->arg = arg;
        st->arg2 = parseFactor(ps);
        CHECK_ERROR();
        st->operator = SOPE_POWER;
    }

    return st;
}

SyntaxTree *parseFactor(ParserState *ps) {
    if (peekType(ps) == TOKEN_PLUS || peekType(ps) == TOKEN_MINUS) {
        if (peekType(ps) == TOKEN_PLUS) {
            nextToken(ps);
            return parseFactor(ps);
        }
        SyntaxTree *st;
        st = allocSyntaxTree(ps);
        st->type = SYNTAX_OPERATION_UNARY;
        st->operator = convertTokenTypeToOperator(peekType(ps));
        nextToken(ps);
        st->arg = parseFactor(ps);
        CHECK_ERROR();
        st->arg2 = NULL;
        return st;
    }
    return parsePower(ps);
}

SyntaxTree *parseTerm(ParserState *ps)
{
    SyntaxTree *st = parseFactor(ps);

    CHECK_ERROR();

    while (isTermOperator(peekType(ps))) {
        SyntaxOperator ope = convertTokenTypeToOperator(peekType(ps));
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseFactor(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->operator = ope;
    }

    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }

    return st;
}

SyntaxTree *parseArith(ParserState *ps)
{
    SyntaxTree *st = parseTerm(ps);
    CHECK_ERROR();
    while (peekType(ps) == TOKEN_PLUS || peekType(ps) == TOKEN_MINUS) {
        SyntaxOperator ope = convertTokenTypeToOperator(peekType(ps));
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseTerm(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->operator = ope;
    }
    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }
    return st;
}

SyntaxTree *parseShift(ParserState *ps)
{
    SyntaxTree *st = parseArith(ps);
    CHECK_ERROR();
    // Fixme Handle shift
    return st;
}

SyntaxTree *parseAndExpr(ParserState *ps)
{
    SyntaxTree *st = parseShift(ps);
    CHECK_ERROR();
    // Fixme Handle bitwise and
    return st;
}

SyntaxTree *parseXor(ParserState *ps)
{
    SyntaxTree *st = parseAndExpr(ps);
    CHECK_ERROR();
    // Fixme Handle xor
    return st;
}

SyntaxTree *parseExpr(ParserState *ps)
{
    SyntaxTree *st = parseXor(ps);
    CHECK_ERROR();
    // Fixme Handle xor
    return st;
}

SyntaxTree *parseComparison(ParserState *ps)
{
    SyntaxTree *st = parseExpr(ps);
    CHECK_ERROR();

    // FIXME : not in and in not not handled here
    while (isComparisonOperator(peekType(ps))) {
        SyntaxOperator ope = convertTokenTypeToOperator(peekType(ps));
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseExpr(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->operator = ope;
    }

    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }

    return st;
}

SyntaxTree *parseNot(ParserState *ps)
{
    if (peekType(ps) == TOKEN_KEYWORD && peekToken(ps)->keyword == KEYWORD_NOT) {
        nextToken(ps);
        SyntaxTree *arg = parseNot(ps);
        SyntaxTree *st = allocSyntaxTree(ps);
        st->arg = arg;
        st->type = SYNTAX_OPERATION_UNARY;
        st->operator = SOPE_NOT;
        return st;
    }

    return parseComparison(ps);
}

SyntaxTree *parseAnd(ParserState *ps)
{
    SyntaxTree *st = parseNot(ps);
    CHECK_ERROR();

    if (peekType(ps) == TOKEN_KEYWORD && peekToken(ps)->keyword == KEYWORD_AND) {
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseNot(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->operator = SOPE_AND;
    }

    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }
    return st;
}

SyntaxTree *parseOr(ParserState *ps)
{
    SyntaxTree *st = parseAnd(ps);
    CHECK_ERROR();

    if (peekType(ps) == TOKEN_KEYWORD && peekToken(ps)->keyword == KEYWORD_OR) {
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseAnd(ps);
        st->type = SYNTAX_OPERATION_BINARY;
        st->operator = SOPE_OR;
    }

    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }
    return st;
}

SyntaxTree *parseTest(ParserState *ps)
{
    SyntaxTree *st = parseOr(ps);
    CHECK_ERROR();

    if (peekType(ps) == TOKEN_KEYWORD
        && peekToken(ps)->keyword == KEYWORD_IF) {
        nextToken(ps);
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->arg = arg;
        st->arg2 = parseOr(ps);
        st->type = SYNTAX_INLINE_IF;
        if (peekType(ps) != TOKEN_KEYWORD
            || (peekType(ps) == TOKEN_KEYWORD && peekToken(ps)->keyword != KEYWORD_ELSE)) {
            PARSING_ERROR(ps, peekToken(ps), "else", "L'opérateur ternaire d'expression nécessite un else.");
            freeSyntaxTree(st);
            return NULL;
        }
        nextToken(ps);
        st->arg3 =  parseOr(ps);
    }

    if (ps->error) {
        freeSyntaxTree(st);
        return NULL;
    }
    return st;
}

SyntaxTree *parseTestStarExpr(ParserState *ps)
{
    SyntaxTree *st = NULL;

    if (peekType(ps) == TOKEN_TIMES) {
        nextToken(ps);
        // TODO
    } else {
        st = parseTest(ps);
    }

    return st;
}

SyntaxTree *parseTestList(ParserState *ps)
{
    NODE_LIST_CODE_EX(Test);
}


SyntaxTree *parseTestListStarExpr(ParserState *ps)
{
    NODE_LIST_CODE_EX(TestStarExpr);
}

SyntaxTree *parseStarExpr(ParserState *ps)
{
    SyntaxTree *st = NULL;

    if (peekType(ps) == TOKEN_TIMES) {
        // TODO
    } else {
        st = parseExpr(ps);
    }

    return st;
}

SyntaxTree *parseExprList(ParserState *ps)
{
    NODE_LIST_CODE_EX(StarExpr);
}

SyntaxTree *parseTestListComp(ParserState *ps)
{
    SyntaxTree *st = parseTestStarExpr(ps);

    CHECK_ERROR();

    if (peekType(ps) == TOKEN_KEYWORD
        && peekToken(ps)->keyword == KEYWORD_FOR) {
        nextToken(ps);
        SyntaxTree *exprloop = st;
        st = allocSyntaxTree(ps);

        st->type = SYNTAX_COMPREHENSION;

        st->arg = exprloop;
        st->arg2 = parseExprList(ps);

        if (ps->error) {
            return NULL;
        }

        if (peekType(ps) != TOKEN_KEYWORD || peekToken(ps)->keyword != KEYWORD_IN) {
            PARSING_ERROR(ps, peekToken(ps), "in", "On doit placer un in après un for.");
            freeSyntaxTree(st);
            return NULL;
        }
        nextToken(ps);

        st->arg3 = parseOr(ps);
    } else {
        SyntaxTree *currentNode = NULL;

        while (peekType(ps) == TOKEN_COMMA) {
            nextToken(ps);
            SyntaxTree *elt = tryParse(ps, parseTestStarExpr);
            if (currentNode == NULL) {
                SyntaxTree *arg = st;
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_FOREST;
                st->arg = arg;
                st->arg2 = elt;
                currentNode = st;
            } else {
                SyntaxTree *parentNode = currentNode;
                currentNode = allocSyntaxTree(ps);
                currentNode->type = SYNTAX_FOREST;
                currentNode->arg = parentNode->arg2;
                currentNode->arg2 = elt;
                parentNode->arg2 = currentNode;
            }
            if (elt == NULL) break;
        }
    }

    return st;
}

SyntaxTree *parseExprStatement(ParserState *ps)
{
    SyntaxTree *st = parseTestListStarExpr(ps);

    CHECK_ERROR();

    if (isAugmentingToken(peekType(ps))) {
        SyntaxTree *arg = st;
        st = allocSyntaxTree(ps);
        st->type = SYNTAX_AUGMENT;
        st->arg = arg;
        st->operator = convertTokenTypeToOperator(peekType(ps));
        nextToken(ps);
        st->arg2 = parseTestList(ps);
    } else {
        SyntaxTree *currentNode = NULL;

        while (peekType(ps) == TOKEN_ASSIGN) {
            nextToken(ps);
            if (currentNode == NULL) {
                SyntaxTree *arg = st;
                st = allocSyntaxTree(ps);
                st->type = SYNTAX_ASSIGN;
                st->arg = arg;
                st->arg2 = parseTestListStarExpr(ps);
                currentNode = st;
            } else {
                SyntaxTree *parentNode = currentNode;
                currentNode = allocSyntaxTree(ps);
                currentNode->type = SYNTAX_ASSIGN;
                currentNode->arg = parentNode->arg2;
                currentNode->arg2 = parseTestListStarExpr(ps);
                parentNode->arg2 = currentNode;
            }
        }
    }

    return st;
}


SyntaxTree *parseSmallStatement(ParserState *ps)
{
    return parseExprStatement(ps);
}

SyntaxTree *parseSimpleStatement(ParserState *ps);

SyntaxTree *parseSuite(ParserState *ps)
{
    SyntaxTree *st;

    if (peekType(ps) == TOKEN_NEWLINE) {
        nextToken(ps);
        // FIXME assert INDENT
        nextToken(ps);
        st = parseStatement(ps);
        SyntaxTree *currentNode = NULL;
        while(peekType(ps) != TOKEN_DEPTH_DECREASE
            && peekType(ps) != TOKEN_EOF) {
            if (currentNode == NULL) {
                currentNode = allocSyntaxTree(ps);
                currentNode->type = SYNTAX_STATEMENTS;
                currentNode->arg = st;
                currentNode->arg2 = parseStatement(ps);
                st = currentNode;
            } else {
                SyntaxTree *parentNode = currentNode;
                currentNode = allocSyntaxTree(ps);
                currentNode->type = SYNTAX_STATEMENTS;
                currentNode->arg = parentNode->arg2;
                currentNode->arg2 = parseStatement(ps);
                parentNode->arg2 = currentNode;
            }
        }
        nextToken(ps);
    } else {
        st = parseSimpleStatement(ps);
    }

    return st;
}

SyntaxTree *parseCompoundStatement(ParserState *ps)
{
    SyntaxTree *st;

    if (peekType(ps) == TOKEN_KEYWORD
        && peekToken(ps)->keyword == KEYWORD_FOR) {
        nextToken(ps);
        st = allocSyntaxTree(ps);
        st->type = SYNTAX_FOR;
        st->arg = parseExprList(ps);
        if (peekType(ps) != TOKEN_KEYWORD || peekToken(ps)->keyword != KEYWORD_IN) {
            PARSING_ERROR(ps, peekToken(ps), "in", "On doit placer un in après le for.");
            freeSyntaxTree(st);
            return NULL;
        }
        nextToken(ps);
        st->arg2 = parseTestList(ps);
        if (peekType(ps) != TOKEN_COLON) {
            PARSING_ERROR(ps, peekToken(ps), ":", "Il faut rajouter un : après le for.");
            freeSyntaxTree(st);
            return NULL;
        }
        nextToken(ps);
        st->arg3 = parseSuite(ps);
    } else if (peekType(ps) == TOKEN_KEYWORD
        && peekToken(ps)->keyword == KEYWORD_WHILE) {
        nextToken(ps);
        st = allocSyntaxTree(ps);
        st->type = SYNTAX_WHILE;
        st->arg = parseTest(ps);
        if (peekType(ps) != TOKEN_COLON) {
            PARSING_ERROR(ps, peekToken(ps), ":", "Il faut rajouter un : après le while.");
            freeSyntaxTree(st);
            return NULL;
        }
        nextToken(ps);
        st->arg2 = parseSuite(ps);
    }

    return st;
}

SyntaxTree *parseSimpleStatement(ParserState *ps)
{
    SyntaxTree *st = parseSmallStatement(ps);

    CHECK_ERROR();

    if (peekType(ps) == TOKEN_NEWLINE) {
        nextToken(ps);
        return st;
    }

    if(peekType(ps) == TOKEN_SEMICOLON && peekNextType(ps) == TOKEN_NEWLINE) {
        nextToken(ps);
        nextToken(ps);
        return st;
    }

    SyntaxTree *arg = st;
    st = allocSyntaxTree(ps);
    st->type = SYNTAX_STATEMENTS;
    st->arg = arg;
    st->arg2 = NULL;

    SyntaxTree *currentNode = NULL;

    while (peekType(ps) != TOKEN_NEWLINE) {
        if (peekType(ps) != TOKEN_SEMICOLON) {
            PARSING_ERROR(ps, peekToken(ps), ";", "");
            return NULL;
        }
        nextToken(ps);

        if (peekType(ps) == TOKEN_NEWLINE) break;

        SyntaxTree *stmt = parseSmallStatement(ps);

        CHECK_ERROR();

        if (currentNode == NULL) {
            st->arg2 = stmt;
            currentNode = st;
        } else {
            SyntaxTree *parentNode = currentNode;
            currentNode = allocSyntaxTree(ps);
            currentNode->type = SYNTAX_STATEMENTS;
            currentNode->arg = parentNode->arg2;
            currentNode->arg2 = stmt;

            parentNode->arg2 = currentNode;
        }
    }

    return st;
}

bool isStartingCompoundStatement(Token *tok)
{
    return tok->type == TOKEN_KEYWORD
        && (tok->keyword == KEYWORD_FOR || tok->keyword == KEYWORD_WHILE);
}

SyntaxTree *parseStatement(ParserState *ps)
{
    if (isStartingCompoundStatement(peekToken(ps))) {
        return parseCompoundStatement(ps);
    }
    return parseSimpleStatement(ps);
}

SyntaxTree *parseArgument(ParserState *ps)
{
    // FIXME named argument and comprehension
    return parseTest(ps);
}

SyntaxTree *parseArgList(ParserState *ps)
{
    NODE_LIST_CODE_EX(Argument);
}

SyntaxTree *parseSliceOp(ParserState *ps)
{
    SyntaxTree *st = NULL;

    if (peekType(ps) !=- TOKEN_COLON) {
        PARSING_ERROR(ps, peekToken(ps), "", "Il devrait il y avoir un : ici.");
        return NULL;
    }
    nextToken(ps);
    return tryParse(ps, parseTest);
}

SyntaxTree *parseSubscript(ParserState *ps)
{
    SyntaxTree *st = tryParse(ps, parseTest);
    if (st == NULL && peekType(ps) != TOKEN_COLON) {
        PARSING_ERROR(ps, peekToken(ps), "", "Il devrait il y avoir un : ici.");
        return NULL;
    }

    if (peekType(ps) != TOKEN_COLON) return st;

    nextToken(ps);
    SyntaxTree *start = st;
    SyntaxTree *end = tryParse(ps, parseTest);
    SyntaxTree *step = tryParse(ps, parseSliceOp);

    st = allocSyntaxTree(ps);
    st->type = SYNTAX_SLICE;
    st->arg = start;
    st->arg2 = end;
    st->arg3 = step;

    return st;
}

SyntaxTree *parseSubscriptList(ParserState *ps)
{
    NODE_LIST_CODE_EX(Subscript);
}

SyntaxTree *parseInput(ParserState *ps)
{
    SyntaxTree *st = NULL;
    SyntaxTree *currentNode = NULL;

    while (peekType(ps) != TOKEN_EOF)
    {
        if (peekType(ps) == TOKEN_NEWLINE) {
            nextToken(ps);
            continue;
        }
        SyntaxTree *stmt = parseStatement(ps);

        if (ps->error) {
            freeSyntaxTree(stmt);
            freeSyntaxTree(st);
            return NULL;
        }

        if (st == NULL) {
            st = stmt;
        } else if (currentNode == NULL) {
            SyntaxTree *arg = st;
            st = allocSyntaxTree(ps);
            st->type = SYNTAX_STATEMENTS;
            st->arg = arg;
            st->arg2 = stmt;
            currentNode = st;
        } else {
            SyntaxTree *parentNode = currentNode;
            currentNode = allocSyntaxTree(ps);
            currentNode->type = SYNTAX_STATEMENTS;
            currentNode->arg = parentNode->arg2;
            currentNode->arg2 = stmt;
            parentNode->arg2 = currentNode;
        }
    }

    return st;
}