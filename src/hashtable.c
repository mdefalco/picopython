//
// Created by marc on 08/11/18.
//
#include <string.h>
#include <stdlib.h>
#include "hashtable.h"

// Hash function djb2 by Dan Bernstein
unsigned long
hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}


HashTable hashTableInit()
{
    HashTable h = (HashTable) malloc(sizeof(HashTableList*) * HASHTABLE_SIZE);

    memset(h, 0, sizeof(HashTableList*) * HASHTABLE_SIZE);

    return h;
}

void hashTableDestroy(HashTable h)
{
    // FIXME
    free(h);
}

void hashTableInsert(HashTable h, unsigned char *key, void *value)
{
    unsigned long hashedKey = hash(key) % HASHTABLE_SIZE;
    HashTableList *node = (HashTableList*)malloc(sizeof(HashTableList));
    node->key = key;
    node->value = value;
    node->next = NULL;

    if (h[hashedKey] == NULL) {
        h[hashedKey] = node;
    } else {
        node->next = h[hashedKey];
        h[hashedKey] = node;
    }
}


bool hashTableTest(HashTable h, unsigned char *key)
{
    unsigned long hashedKey = hash(key) % HASHTABLE_SIZE;
    HashTableList *node = h[hashedKey];
    while (node != NULL) {
        if (strcmp(key, node->key) == 0) return true;
        node = node->next;
    }

    return false;
}

void *hashTableUpdate(HashTable h, unsigned char *key, void *value)
{
    unsigned long hashedKey = hash(key) % HASHTABLE_SIZE;
    HashTableList *node = h[hashedKey];
    while (node != NULL) {
        if (strcmp(key, node->key) == 0) {
            void *previousValue = node->value;
            node->value = value;
            return previousValue;
        }
        node = node->next;
    }

    // Not present so insert key :
    hashTableInsert(h, key, value);

    return NULL;
}

void *hashTableGet(HashTable h, unsigned char *key)
{
    unsigned long hashedKey = hash(key) % HASHTABLE_SIZE;
    HashTableList *node = h[hashedKey];
    while (node != NULL) {
        if (strcmp(key, node->key) == 0) return node->value;
        node = node->next;
    }

    return NULL;
}
