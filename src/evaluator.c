//
// Created by marc on 08/11/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "evaluator.h"

bool isZero(PythonValue v)
{
    if (v.type == VALUE_INTEGER && v.integer == 0)
        return true;

    if (v.type == VALUE_FLOAT && v.floating == 0)
        return true;

    if ((v.type == VALUE_TUPLE || v.type == VALUE_LIST) && v.seq->length == 0)
        return true;

    if (v.type == VALUE_STRING && v.string->length == 0)
        return true;

    if (v.type == VALUE_NONE)
        return true;

    return false;
}

PythonValueSequence* initValueSequence(unsigned int length)
{
    PythonValueSequence *seq = (PythonValueSequence *)malloc(sizeof(PythonValueSequence));
    seq->length = length;
    seq->elements = (PythonValue *)malloc(sizeof(PythonValue) * length);
    seq->current = 0;
}

PythonValueString *initValueString(const char *constant)
{
    PythonValueString *str = (PythonValueString *)malloc(sizeof(PythonValueString));
    str->length = strlen(constant);
    str->current = 0;
    str->elements = strdup(constant);
    return str;
}

PythonValueString *initEmptyValueString(unsigned int length)
{
    PythonValueString *str = (PythonValueString *)malloc(sizeof(PythonValueString));
    str->length = length;
    str->current = 0;
    str->elements = (char *)malloc(sizeof(char) * (length + 1));
    str->elements[length] = '\0';
    return str;
}

bool isSequence(PythonValue v)
{
    PythonType t = v.type;
    return t == VALUE_TUPLE || t == VALUE_LIST;
}

bool isIterable(PythonValue v)
{
    PythonType t = v.type;
    return t == VALUE_TUPLE || t == VALUE_LIST | t == VALUE_RANGE | t == VALUE_STRING;
}

bool canCoerceTo(PythonValue v, PythonValue tgt)
{
    if (v.type == tgt.type) return true;

    switch(tgt.type) {
        case VALUE_FLOAT:
            return v.type == VALUE_INTEGER;
        default:
            break;
    }

    return false;
}

PythonValue coerceTo(PythonValue v, PythonValue tgt)
{
    PythonValue ret;

    if (v.type == tgt.type) return v;

    ret.type = tgt.type;

    switch(tgt.type) {
        case VALUE_FLOAT:
            if (v.type == VALUE_INTEGER)
                ret.floating  = (float) v.integer;
            break;
        default:
            break;
    }

    return ret;
}

int evaluateComparisonString(SyntaxOperator op, PythonValueString *op1, PythonValueString *op2)
{
    int n = strcmp(op1->elements, op2->elements);
    switch(op) {
        case SOPE_LESS:
            return (n < 0) ? 1 : 0;
        case SOPE_GREATER:
            return (n > 0) ? 1 : 0;
        case SOPE_LESS_EQUAL:
            return (n <= 0) ? 1 : 0;
        case SOPE_GREATER_EQUAL:
            return (n >= 0) ? 1 : 0;
        case SOPE_EQUALS:
            return (n == 0) ? 1 : 0;
        case SOPE_DIFF:
            return (n != 0) ? 1 : 0;
        default:
            break;
    }
    return -1; // out of reach
}

PythonValue evaluateBinaryOperation(SyntaxOperator op, PythonValue op1, PythonValue op2);

int pythonValueSequenceCmp(PythonValueSequence *op1, PythonValueSequence *op2)
{
    int i = 0;
    while(i < op1->length && i < op2->length) {
        if (evaluateBinaryOperation(SOPE_EQUALS, op1->elements[i], op2->elements[i]).integer == 0) {
            if (evaluateBinaryOperation(SOPE_LESS, op1->elements[i], op2->elements[i]).integer == 1) {
                return -1;
            } else {
                return 1;
            }
        }

        i++;
    }

    if (op1->length < op2->length) return 1;
    if (op2->length < op1->length) return -1;
    return 0;
}

int evaluateComparisonSequence(SyntaxOperator op, PythonValueSequence *op1, PythonValueSequence *op2)
{
    int n = pythonValueSequenceCmp(op1, op2);
    switch(op) {
        case SOPE_LESS:
            return (n < 0) ? 1 : 0;
        case SOPE_GREATER:
            return (n > 0) ? 1 : 0;
        case SOPE_LESS_EQUAL:
            return (n <= 0) ? 1 : 0;
        case SOPE_GREATER_EQUAL:
            return (n >= 0) ? 1 : 0;
        case SOPE_EQUALS:
            return (n == 0) ? 1 : 0;
        case SOPE_DIFF:
            return (n != 0) ? 1 : 0;
        default:
            break;
    }
    return -1; // out of reach
}

int evaluateComparisonInteger(SyntaxOperator op, int op1, int op2)
{
    switch(op) {
        case SOPE_LESS:
            return (op1 < op2) ? 1 : 0;
        case SOPE_GREATER:
            return (op1 > op2) ? 1 : 0;
        case SOPE_LESS_EQUAL:
            return (op1 <= op2) ? 1 : 0;
        case SOPE_GREATER_EQUAL:
            return (op1 >= op2) ? 1 : 0;
        case SOPE_EQUALS:
            return (op1 == op2) ? 1 : 0;
        case SOPE_DIFF:
            return (op1 != op2) ? 1 : 0;
        default:
            break;
    }
    return -1; // out of reach
}

int evaluateComparisonFloat(SyntaxOperator op, float op1, float op2)
{
    switch(op) {
        case SOPE_LESS:
            return (op1 < op2) ? 1 : 0;
        case SOPE_GREATER:
            return (op1 > op2) ? 1 : 0;
        case SOPE_LESS_EQUAL:
            return (op1 <= op2) ? 1 : 0;
        case SOPE_GREATER_EQUAL:
            return (op1 >= op2) ? 1 : 0;
        case SOPE_EQUALS:
            return (op1 == op2) ? 1 : 0;
        case SOPE_DIFF:
            return (op1 != op2) ? 1 : 0;
        default:
            break;
    }
    return -1; // out of reach
}

int integerPower(int op1, int op2)
{
    int p = 1;
    for (int i = 0; i < op2; i++)
        p *= op1;
    return p;
}

int evaluateBinaryOperationInteger(SyntaxOperator op, int op1, int op2)
{
    switch(op) {
        case SOPE_TIMES: return op1 * op2;
        case SOPE_PLUS: return op1 + op2;
        case SOPE_MINUS: return op1 - op2;
        case SOPE_DIVIDE: return op1 / op2;
        case SOPE_MODULO: return op1 % op2;
        case SOPE_POWER: return integerPower(op1, op2);
        default:
            break;
    }
    return -1; // out of reach
}

float evaluateBinaryOperationFloat(SyntaxOperator op, float op1, float op2)
{
    switch(op) {
        case SOPE_TIMES: return op1 * op2;
        case SOPE_PLUS: return op1 + op2;
        case SOPE_MINUS: return op1 - op2;
        case SOPE_DIVIDE: return op1 / op2;
        case SOPE_TRUEDIVIDE: return op1 / op2;
        case SOPE_POWER: return pow(op1, op2);
        default:
            break;
    }
    return -1.0; // out of reach
}

PythonValue evaluateUnaryOperation(SyntaxOperator op, PythonValue ope)
{
    PythonValue ret;
    ret.type = VALUE_NONE;

    switch(op) {
        case SOPE_MINUS:
            if (ope.type == VALUE_INTEGER) {
                ret.type = VALUE_INTEGER;
                ret.integer = -ope.integer;
            }
            if (ope.type == VALUE_FLOAT) {
                ret.type = VALUE_FLOAT;
                ret.floating = -ope.floating;
            }
            break;
        case SOPE_NOT:
            ret.type = VALUE_INTEGER;
            ret.integer = !isZero(ope);
            break;
        default:
            break;
    }

    return ret;
}

PythonValue evaluateBinaryOperation(SyntaxOperator op,
    PythonValue op1, PythonValue op2)
{
    PythonValue ret;
    ret.type = VALUE_NONE;

    switch(op) {
        case SOPE_AND:
            ret.type = VALUE_INTEGER;
            ret.integer = !isZero(op1) && !isZero(op2);
            break;
        case SOPE_OR:
            ret.type = VALUE_INTEGER;
            ret.integer = !isZero(op1) || !isZero(op2);
            break;
        case SOPE_TIMES:
        case SOPE_PLUS:
        case SOPE_MINUS:
        case SOPE_POWER:
            if (op1.type == VALUE_INTEGER && op2.type == VALUE_INTEGER && op2.integer >= 0) {
                ret.type = VALUE_INTEGER;
                ret.integer = evaluateBinaryOperationInteger(op, op1.integer, op2.integer);
            } else if ( (op1.type == VALUE_FLOAT && canCoerceTo(op2,op1))
                    || (op2.type == VALUE_FLOAT && canCoerceTo(op1,op2))) {
                PythonValue fop1 = op1;
                PythonValue fop2 = op2;
                if (op1.type == VALUE_FLOAT && canCoerceTo(op2, op1)) {
                    fop2 = coerceTo(op2, op1);
                } else {
                    fop1 = coerceTo(op1, op2);
                }
                ret.type = VALUE_FLOAT;
                ret.floating = evaluateBinaryOperationFloat(op, fop1.floating, fop2.floating);
            } else if (op1.type == VALUE_STRING && op2.type == VALUE_STRING) {
                ret.type = VALUE_STRING;
                ret.string = initEmptyValueString(op1.string->length + op2.string->length);
                memcpy(ret.string->elements, op1.string->elements, op1.string->length);
                memcpy(ret.string->elements+op1.string->length,
                        op2.string->elements, op2.string->length);
            } else if (isSequence(op1) && isSequence(op2) && op1.type == op2.type) {
                ret.type = op1.type;
                ret.seq = initValueSequence(op1.seq->length + op2.seq->length);
                memcpy(ret.seq->elements, op1.seq->elements, sizeof(PythonValue)*op1.seq->length);
                memcpy(ret.seq->elements+op1.seq->length,
                        op2.seq->elements, sizeof(PythonValue)*op2.seq->length);
            }
            // TODO
            break;
        case SOPE_DIVIDE:
            if (op1.type == VALUE_INTEGER && op2.type == VALUE_INTEGER) {
                ret.type = VALUE_INTEGER;
                ret.integer = op1.integer / op2.integer;
            } // TODO
            break;
        case SOPE_TRUEDIVIDE:
            if (op1.type == VALUE_INTEGER && op2.type == VALUE_INTEGER) {
                ret.type = VALUE_FLOAT;
                ret.floating = evaluateBinaryOperationFloat(op, op1.integer, op2.integer);
            } else if (op1.type == VALUE_FLOAT || op2.type == VALUE_FLOAT) {
                PythonValue fop1 = op1;
                PythonValue fop2 = op2;
                if (op1.type == VALUE_FLOAT && canCoerceTo(op2, op1)) {
                    fop2 = coerceTo(op2, op1);
                } else if (op2.type == VALUE_FLOAT && canCoerceTo(op1, op2)) {
                    fop1 = coerceTo(op1, op2);
                } else
                    break; // TODO typeerror

                ret.type = VALUE_FLOAT;
                ret.floating = evaluateBinaryOperationFloat(op, fop1.floating, fop2.floating);
            } // TODO
            break;
        case SOPE_EQUALS:
        case SOPE_DIFF:
        case SOPE_LESS:
        case SOPE_GREATER:
        case SOPE_LESS_EQUAL:
        case SOPE_GREATER_EQUAL:
            if (op1.type == VALUE_INTEGER && op2.type == VALUE_INTEGER) {
                ret.type = VALUE_INTEGER;
                ret.integer = evaluateComparisonInteger(op, op1.integer, op2.integer);
            } else if ( (op1.type == VALUE_FLOAT && canCoerceTo(op2,op1))
                        || (op2.type == VALUE_FLOAT && canCoerceTo(op1,op2))) {
                PythonValue fop1 = op1;
                PythonValue fop2 = op2;
                if (op1.type == VALUE_FLOAT) {
                    fop2 = coerceTo(op2, op1);
                } else {
                    fop1 = coerceTo(op1, op2);
                }
                ret.type = VALUE_INTEGER;
                ret.integer = evaluateComparisonFloat(op, fop1.floating, fop2.floating);
            } else if (op1.type == VALUE_STRING && op2.type == VALUE_STRING) {
                ret.type = VALUE_INTEGER;
                ret.integer = evaluateComparisonString(op, op1.string, op2.string);
            } else if (isSequence(op1) && isSequence(op2) && op1.type == op2.type) {
                ret.type = VALUE_INTEGER;
                ret.integer = evaluateComparisonSequence(op, op1.seq, op2.seq);
            }
            break;
        default:
            break;
    }

    return ret;
}



void builtin_type(PythonValue v, PythonValue *ret)
{
    char *type = "Unknown type";
    ret->type = VALUE_STRING;
    if (v.seq->elements[0].type == VALUE_INTEGER)
        type = "int";
    else if (v.seq->elements[0].type == VALUE_FLOAT)
        type = "float";
    else if (v.seq->elements[0].type == VALUE_STRING)
        type = "str";
    else if (v.seq->elements[0].type == VALUE_TUPLE)
        type = "tuple";
    else if (v.seq->elements[0].type == VALUE_LIST)
        type = "list";
    else if (v.seq->elements[0].type == VALUE_NONE)
        type = "NoneType";
    ret->string = initValueString(type);
}

void builtin_print(PythonValue v, PythonValue *ret)
{
    if (v.type != VALUE_TUPLE) {
        pprint_value(v);
    } else {
        for (int i = 0; i < v.seq->length; i++) {
            pprint_value(v.seq->elements[i]);
            if (i != v.seq->length - 1) putchar(' ');
        }
    }
    putchar('\n');
}

void builtin_len(PythonValue v, PythonValue *ret)
{
    if (v.type != VALUE_TUPLE) {
        return; // fixme type error
    }
    ret->type = VALUE_INTEGER;
    ret->integer = v.seq->length;
}

void builtin_range(PythonValue v, PythonValue *ret)
{
    ret->type = VALUE_RANGE;
    ret->rangeData = (void *)malloc(sizeof(struct builtins_range));
    ret->rangeData->start = 0;
    ret->rangeData->stop = 0;
    ret->rangeData->step = 1;

    int nargs = v.seq->length;
    // FIXME assert that all items are integer values and args is a TUPLE
    if (nargs == 1) {
        ret->rangeData->stop = v.seq->elements[0].integer;
    } else if (nargs == 2) {
        ret->rangeData->start = v.seq->elements[0].integer;
        ret->rangeData->stop = v.seq->elements[1].integer;
    } else if (nargs == 3) {
        ret->rangeData->start = v.seq->elements[0].integer;
        ret->rangeData->stop = v.seq->elements[1].integer;
        ret->rangeData->step = v.seq->elements[2].integer;
    }
}

#define MIN(a,b) (a<b)?a:b
#define MAX(a,b) (a>b)?a:b

unsigned int iterableCount(PythonValue v)
{
    if (v.type == VALUE_RANGE) {
        return MAX((v.rangeData->stop - v.rangeData->start) / v.rangeData->step, 0);
        // FIXME with step and rounding
    }
    if (v.type == VALUE_TUPLE || v.type == VALUE_LIST)
        return v.seq->length;
    if (v.type == VALUE_STRING)
        return v.string->length;

    return (unsigned int)-1; // should not be reached
}

void iterableInit(PythonValue v)
{
    if (v.type == VALUE_RANGE)
        v.rangeData->current = v.rangeData->start;
    if (v.type == VALUE_TUPLE || v.type == VALUE_LIST)
        v.seq->current = 0;
    if (v.type == VALUE_STRING)
        v.string->current = 0;
}

PythonValue *iterableNext(PythonValue v)
{
    if (v.type == VALUE_RANGE) {
        if (v.rangeData->current >= v.rangeData->stop)
            return NULL;

        PythonValue *ret = (PythonValue *)malloc(sizeof(PythonValue));
        ret->type = VALUE_INTEGER;
        ret->integer = v.rangeData->current;
        v.rangeData->current += v.rangeData->step;
        return ret;
    } else if (v.type == VALUE_TUPLE || v.type == VALUE_LIST) {
        if (v.seq->current >= v.seq->length) return NULL;
        return &v.seq->elements[v.seq->current++];
    } else if (v.type == VALUE_STRING) {
        if (v.string->current >= v.string->length) return NULL;
        char c = v.string->elements[v.string->current++];
        PythonValue *ret = (PythonValue *)malloc(sizeof(PythonValue));
        ret->type = VALUE_STRING;
        ret->string = initEmptyValueString(1);
        ret->string->elements[0] = c;
        return ret;
    }

    return NULL;
}


void builtin_list(PythonValue vargs, PythonValue *ret) {
    PythonValue v = vargs.seq->elements[0];
    ret->type = VALUE_LIST;
    unsigned int length = iterableCount(v);

    ret->seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
    ret->seq->elements = (PythonValue *)malloc(sizeof(PythonValue) * length);
    ret->seq->length = length;

    iterableInit(v);
    PythonValue *next = NULL;
    for (int i = 0; i < length; i++)
    {
       next = iterableNext(v);
       ret->seq->elements[i] = *next;
    }
}

unsigned int forestSize(SyntaxTree *st) {
    if (st->arg == NULL) return 0;

    unsigned int n = 0;
    SyntaxTree *node = st;
    n = 1;
    while (node->arg2 != NULL && node->arg2->type == SYNTAX_FOREST) {
        n++;
        node = node->arg2;
    }
    if (node->arg2 != NULL) n++;
    return n;
}

SyntaxTree **forestFlatten(SyntaxTree *st) {
    unsigned int n = forestSize(st);
    if (n == 0) return NULL;

    SyntaxTree **a = (SyntaxTree **) malloc(sizeof(SyntaxTree * ) * n);

    unsigned int i = 0;
    SyntaxTree *node = st;
    a[i] = node->arg;
    while (node->arg2 != NULL && node->arg2->type == SYNTAX_FOREST) {
        i++;
        node = node->arg2;
        a[i] = node->arg;
    }
    if (node->arg2 != NULL) {
        a[i+1] = node->arg2;
    }
    return a;
}

PythonValue *environmentLookup(Environment env, char *name) {
    PythonValue *val = NULL;
    if (hashTableTest(env.locals, name)) {
        val = (PythonValue *) hashTableGet(env.locals, name);
    } else if (hashTableTest(env.globals, name)) {
        val = (PythonValue *) hashTableGet(env.globals, name);
    } else if (hashTableTest(env.builtins, name)) {
        val = (PythonValue *) hashTableGet(env.builtins, name);
    } else { /* FIXME Nameerror */ }
    return val;
}

void evaluateAssign(Environment env, SyntaxTree *var, PythonValue val)
{
    if (var->type == SYNTAX_FOREST) {
        if (val.type == VALUE_TUPLE || val.type == VALUE_LIST) {
            unsigned int nvars = forestSize(var);
            if (val.seq->length == nvars) {
                SyntaxTree **vars = forestFlatten(var);
                for (int i = 0; i < nvars; i++) {
                    evaluateAssign(env, vars[i], val.seq->elements[i]);
                }
                free(vars);
            }
        }
    } else if (var->type == SYNTAX_TUPLE) {
        // a Syntax Tuple has always a syntax forest for only child
        evaluateAssign(env, var->arg, val);
    } else if (var->type == SYNTAX_IDENTIFIER) {
        PythonValue *box = environmentLookup(env, var->text);
        if (box != NULL) {
            *box = val;
        } else {
            // New variable
            box = (PythonValue *) malloc(sizeof(PythonValue));
            *box = val;
            hashTableUpdate(env.locals, var->text, (void *)box);
        }
    } else if (var->type == SYNTAX_GETITEM) {
        PythonValue iter = evaluate(env, var->arg);
        if (iter.type == VALUE_LIST) {
            if (var->arg2->type == SYNTAX_SLICE) {
                // slice assign TODO
            } else {
                // direct assign
                PythonValue index = evaluate(env, var->arg2);
                if (index.type == VALUE_INTEGER) {
                    int n = index.integer;
                    if (n < 0) n += iter.seq->length;
                    if (n < 0 || n >= iter.seq->length) {
                        // FIXME
                    } else {
                        iter.seq->elements[n] = val;
                    }
                }
            }
        } else {
            // FIXME
        }
    }
}

PythonValue evaluateForest(Environment env, SyntaxTree *st)
{
    PythonValue ret;
    ret.type = VALUE_TUPLE;
    int n = 0;
    if (st->arg != NULL) {
        SyntaxTree *node = st;
        n = 1;
        while (node->arg2 != NULL && node->arg2->type == SYNTAX_FOREST) {
            n++;
            node = node->arg2;
        }
        if (node->arg2 != NULL) n++;
    }
    ret.seq = (PythonValueSequence *)malloc(sizeof(PythonValueSequence));
    if (n > 0)
        ret.seq->elements = (PythonValue *)malloc(sizeof(PythonValue)*n);
    else
        ret.seq->elements = NULL;
    ret.seq->length = n;

    if (st->arg != NULL) {
        SyntaxTree *node = st;
        ret.seq->elements[0] = evaluate(env, st->arg);
        int i = 0;
        while (node->arg2 != NULL && node->arg2->type == SYNTAX_FOREST) {
            node = node->arg2;
            i++;
            ret.seq->elements[i] = evaluate(env, node->arg);
        }
        if (node->arg2 != NULL) {
            ret.seq->elements[i+1] = evaluate(env, node->arg2);
        }
    }
    return ret;
}

PythonValue tupleSingleton(PythonValue x)
{
    PythonValue ret;
    ret.type = VALUE_TUPLE;
    ret.seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
    ret.seq->length = 1;
    ret.seq->elements = (PythonValue *)malloc(sizeof(PythonValue));
    *ret.seq->elements = x;
    return ret;
}

PythonValue evaluate(Environment env, SyntaxTree *st)
{
    PythonValue ret;
    ret.type = VALUE_NONE;

    if (st == NULL) return ret;

    switch(st->type)
    {
        case SYNTAX_STATEMENTS:
            ret = evaluate(env, st->arg);
            if (st->arg2 != NULL) {
                ret = evaluate(env, st->arg2);
            }
            break;
        case SYNTAX_AUGMENT: {
            // Syntactic sugar x op= y -> x = x op y
            // FIXME
            st->type = SYNTAX_OPERATION_BINARY;
            PythonValue val = evaluate(env, st);
            evaluateAssign(env, st->arg, val);
            st->type = SYNTAX_AUGMENT;
            break;
        }
        case SYNTAX_ASSIGN: {
            PythonValue val = evaluate(env, st->arg2);
            evaluateAssign(env, st->arg, val);
            break;
            }
        case SYNTAX_OPERATION_UNARY: {
            PythonValue op = evaluate(env, st->arg);
            ret = evaluateUnaryOperation(st->operator, op);
            break;
        }
        case SYNTAX_OPERATION_BINARY: {
            PythonValue op1 = evaluate(env, st->arg);
            PythonValue op2 = evaluate(env, st->arg2);
            ret = evaluateBinaryOperation(st->operator, op1, op2);
            break;
        }
        case SYNTAX_CONSTANT_STRING:
            ret.type = VALUE_STRING;
            ret.string = initValueString(st->text);
            break;
        case SYNTAX_CONSTANT_INTEGER:
            ret.type = VALUE_INTEGER;
            ret.integer = st->integer;
            break;
        case SYNTAX_CONSTANT_FLOAT:
            ret.type = VALUE_FLOAT;
            ret.floating = st->floating;
            break;
        case SYNTAX_IDENTIFIER:
            ret = *environmentLookup(env, st->text);
            break;
        case SYNTAX_GETITEM: {
            PythonValue l = evaluate(env, st->arg);
            if (isIterable(l)) {
                if (st->arg2->type == SYNTAX_SLICE) {
                    PythonValue start = evaluate(env, st->arg2->arg);
                    PythonValue stop = evaluate(env, st->arg2->arg2);
                    PythonValue step = evaluate(env, st->arg2->arg3);

                    unsigned int length = l.seq->length;

                    if (start.type == VALUE_NONE) {
                        start = (PythonValue) { VALUE_INTEGER, 0 };
                    }

                    if (stop.type == VALUE_NONE) {
                        stop = (PythonValue) { VALUE_INTEGER, (size_t)length };
                    }

                    if (step.type == VALUE_NONE) {
                        step = (PythonValue) { VALUE_INTEGER, (size_t)1 };
                    }

                    if (start.type == VALUE_INTEGER
                        && stop.type == VALUE_INTEGER && step.type == VALUE_INTEGER) {
                        int istart = start.integer;
                        int istop = stop.integer;
                        int istep = step.integer;

                        istart = (istart < 0) ? istart + length : istart;
                        istop = (istop < 0) ? istop + length : istop;

                        if (l.type == VALUE_RANGE) {
                            int rangeSize = (l.rangeData->stop - l.rangeData->start) / l.rangeData->step; // FIXME
                            if (istop < 0) istop += rangeSize;
                            if (istart < 0) istart += rangeSize;
                            ret.rangeData = (struct builtins_range *) malloc(sizeof(struct builtins_range));
                            ret.rangeData->start = MAX(istart, l.rangeData->start);
                            ret.rangeData->stop = MIN(istop, l.rangeData->stop);
                            ret.rangeData->step = istep;
                        } else {
                            int nitems = 0;
                            if (istep > 0) { // FIXME
                                for (int i = istart; i < istop; i += istep) {
                                    nitems++;
                                }

                                ret.type = l.type;
                                if (l.type == VALUE_STRING) {
                                    ret.string = initEmptyValueString(nitems);
                                } else {
                                    ret.seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
                                    ret.seq->elements = (PythonValue *) malloc(sizeof(PythonValue) * nitems);
                                    ret.seq->length = nitems;
                                }

                                int j = 0;
                                for (int i = istart; i < istop; i += istep) {
                                    if (l.type == VALUE_STRING) {
                                        ret.string->elements[j] = l.string->elements[i];
                                    } else {
                                        ret.seq->elements[j] = l.seq->elements[i];
                                    }
                                    j++;
                                }
                            }
                        }
                    }
                } else {
                    PythonValue sub = evaluate(env, st->arg2);
                    if (sub.type == VALUE_INTEGER) {
                        int index = sub.integer;
                        index = (index < 0) ? index + iterableCount(l) : index;
                        if (l.type == VALUE_RANGE) {
                            // FIXME
                        } else if (l.type == VALUE_STRING) {
                            ret.type = VALUE_STRING;
                            ret.string = initEmptyValueString(1);
                            ret.string->elements[0] = l.string->elements[index];
                        } else {
                            ret = l.seq->elements[index];
                        }
                    } else {
                        // FIXME
                    }
                }
            }
            break;
        }
        case SYNTAX_TUPLE:
        case SYNTAX_LIST: {
            ret.type = (st->type == SYNTAX_LIST) ?  VALUE_LIST : VALUE_TUPLE;
            if (st->arg != NULL) {
                if (st->arg->type == SYNTAX_FOREST) {
                    PythonValue varg = evaluateForest(env, st->arg);
                    ret.seq = varg.seq;
                } else if (st->arg->type == SYNTAX_COMPREHENSION) {
                    PythonValue iterable = evaluate(env, st->arg->arg3);
                    unsigned int length = iterableCount(iterable);
                    // FIXME assert iterable
                    ret.seq = (PythonValueSequence *)malloc(sizeof(PythonValueSequence));
                    ret.seq->elements = (PythonValue *)malloc(sizeof(PythonValue) * length);
                    ret.seq->length = length;
                    const char *varcomp = st->arg->arg2->text; // FIXME
                    PythonValue *current = iterableNext(iterable);
                    int i = 0;
                    while (current != NULL) {
                        hashTableUpdate(env.locals, varcomp, (void*)current);
                        ret.seq->elements[i] = evaluate(env, st->arg->arg);
                        i++;
                        current = iterableNext(iterable);
                    }
                } else {
                    ret.seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
                    ret.seq->length = 1;
                    ret.seq->elements = (PythonValue *)malloc(sizeof(PythonValue));
                    *ret.seq->elements = evaluate(env, st->arg);
                }
            } else {
                ret.seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
                ret.seq->length = 0;
            }
            break;
        }

        case SYNTAX_FOREST: {
            // Perhaps, we should put this case in the parser to be sure to enclosed this in a syntax_tuple
            PythonValue varg = evaluateForest(env, st);
            ret.type = VALUE_TUPLE;
            ret.seq = varg.seq;
            break;
        }

        case SYNTAX_CALL: {
            PythonValue function = evaluate(env, st->arg);
            PythonValue args;

            if (st->arg2 != NULL) {
                if (st->arg2->type == SYNTAX_FOREST) {
                    args = evaluateForest(env, st->arg2);
                } else {
                    args = tupleSingleton(evaluate(env, st->arg2));
                }
            } else {
                args.type = VALUE_TUPLE;
                args.seq = (PythonValueSequence *) malloc(sizeof(PythonValueSequence));
                args.seq->length = 0;
            }

            if (function.type == VALUE_BUILTIN) {
                function.builtin_function(args, &ret);
            }
            // FIXME
            break;
        }
        case SYNTAX_WHILE: {
            while (!isZero(evaluate(env, st->arg))) {
                evaluate(env, st->arg2);
            }
            break;
        }
        case SYNTAX_FOR: {
            /*
            if (st->arg->type != SYNTAX_IDENTIFIER)
                // FIXME Syntax error
            */
            SyntaxTree *var = st->arg;
            PythonValue iterable = evaluate(env, st->arg2);
            iterableInit(iterable);
            PythonValue *next = iterableNext(iterable);

            while (next != NULL) {
                evaluateAssign(env, var, *next);
                evaluate(env, st->arg3);
                next = iterableNext(iterable);
            }

            break;
        }
        default:
            break;
    }

    return ret;
};

void pprint_value(PythonValue v)
{
    switch(v.type) {
        case VALUE_STRING:
            printf("%s", v.string->elements);
            break;
        case VALUE_FLOAT:
            printf("%f", v.floating);
            break;
        case VALUE_INTEGER:
            printf("%d", v.integer);
            break;
        case VALUE_LIST:
            printf("[");
            for (int i = 0; i < v.seq->length; i++) {
                pprint_value(v.seq->elements[i]);
                if (i < v.seq->length - 1) printf(",");
            }
            printf("]");
            break;
        case VALUE_FUNCTION:
            printf("<function>");
            break;
        case VALUE_TUPLE:
            printf("(");
            for(int i = 0; i < v.seq->length; i++) {
                pprint_value(v.seq->elements[i]);
                if (i < v.seq->length - 1) printf(",");
            }
            printf(")");
            break;
        case VALUE_RANGE:
            printf("range(%d,%d,%d)", v.rangeData->start, v.rangeData->stop, v.rangeData->step);
            break;
        case VALUE_OBJECT:
            printf("<object>");
            break;
        case VALUE_NONE:
            printf("None");
            break;
        default:
            printf("Error value type %d", v.type);
            break;
    }
}

void pprint_hashTable(HashTable env)
{
    printf("{");
    for (int i = 0; i < HASHTABLE_SIZE; i++) {
        HashTableList *elt = env[i];

        while (elt != NULL) {
            printf("%s=",elt->key);
            pprint_value(*(PythonValue*)elt->value);
            printf(", ");
            elt = elt->next;
        }
    }
    printf("}\n");
}

void pprint_env(Environment env)
{
    printf("globals: "); pprint_hashTable(env.globals);
    printf("locals: "); pprint_hashTable(env.locals);
}

void addBuiltinFunction(Environment env, const char *name, void(*function)(PythonValue,PythonValue *))
{
    PythonValue *v = (PythonValue *)malloc(sizeof(PythonValue));
    v->type = VALUE_BUILTIN;
    v->builtin_function = function;
    hashTableUpdate(env.builtins, name, (void*)v);
}

Environment initMainEnvironment() {
    HashTable h = hashTableInit();
    Environment env;

    env.globals = h;
    env.locals = h;
    // FIXME move builtins to a global variable maybe ?
    env.builtins = hashTableInit();

    addBuiltinFunction(env, "range", builtin_range);
    addBuiltinFunction(env, "print", builtin_print);
    addBuiltinFunction(env, "len", builtin_len);
    addBuiltinFunction(env, "list", builtin_list);
    addBuiltinFunction(env, "type", builtin_type);

    return env;
}
