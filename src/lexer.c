//
// Created by marc on 04/11/18.
//
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "lexer.h"

int isident(int c)
{
    return isalnum(c) || c == '_';
}

struct KeywordAssociation {
    char *text;
    Keyword keyword;
};

struct KeywordAssociation keywords[] = {
        { "True", KEYWORD_TRUE },
        { "False", KEYWORD_FALSE },
        { "for", KEYWORD_FOR },
        { "if", KEYWORD_IF },
        { "in", KEYWORD_IN },
        { "else", KEYWORD_ELSE },
        { "and", KEYWORD_AND },
        { "or", KEYWORD_OR },
        { "not", KEYWORD_NOT },
        { "while", KEYWORD_WHILE }
};
unsigned int numKeywords = sizeof(keywords) / sizeof(struct KeywordAssociation);

Keyword convertToKeyword(const char *s)
{
    for (int i = 0; i < numKeywords; i++)
    {
        if (strcmp(keywords[i].text, s) == 0)
            return keywords[i].keyword;
    }

    return -1;
}

int isKeyword(const char *s)
{
    for (int i = 0; i < numKeywords; i++)
    {
        if (strcmp(keywords[i].text, s) == 0) return true;
    }

    return false;
}

/* computes indentation depth for the current line */
unsigned int getDepth(LexerState *ls)
{
    char c;
    int depth = 0;

    while (ls->buf[ls->pos] != '\0')
    {
        c = ls->buf[ls->pos];

        if (c != ' ' && c != '\t') break;
        depth++;

        ls->pos++;
    }

    return depth;
}


Token buildToken(TokenType tt, unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end)
{
    Token tk;
    tk.type = tt;
    tk.pos = pos;
    tk.start = start;
    tk.end = end;
    tk.line = line;
    return tk;
}

Token buildTokenKeyword(unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end, Keyword value)
{
    Token tk = buildToken(TOKEN_KEYWORD,pos,line,start,end);
    tk.keyword = value;
    return tk;
}

Token buildTokenIdentifier(unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end, char *value)
{
    Token tk = buildToken(TOKEN_IDENT,pos,line,start,end);
    tk.text = value;
    return tk;
}

Token buildTokenFloat(unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end, float value)
{
    Token tk = buildToken(TOKEN_FLOAT,pos,line,start,end);
    tk.floating = value;
    return tk;
}

Token buildTokenInteger(unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end, int value)
{
    Token tk = buildToken(TOKEN_INTEGER,pos,line,start,end);
    tk.integer = value;
    return tk;
}

Token buildTokenString(unsigned int pos,
        unsigned int line, unsigned int start, unsigned int end, char *value)
{
    Token tk = buildToken(TOKEN_STRING,pos,line,start,end);
    tk.text = value;
    return tk;
}

void pushToken(Tokens *tokens, Token tok)
{
    if (tokens->arraySize == tokens->numTokens)
    {
        Token *newTokens = (Token *) malloc(sizeof(Token) * tokens->arraySize * 2);
        for (int i = 0; i < tokens->arraySize; i++) {
            newTokens[i] = tokens->tokens[i];
        }
        free(tokens->tokens);
        tokens->tokens = newTokens;
        tokens->arraySize *= 2;
    }
    tokens->tokens[tokens->numTokens] = tok;
    tokens->numTokens++;
}

Tokens initTokens()
{
    Tokens tk;
    unsigned int initial_size = 100;
    tk.tokens = (Token *) malloc( sizeof(Token) * initial_size );
    tk.arraySize = initial_size;
    tk.numTokens = 0;
    return tk;
}

Tokens stringToTokens(char *buf)
{
    Tokens tk = initTokens();

    LexerState ls;
    ls.buf = buf;
    ls.pos = 0;
    ls.depth = 0;

    unsigned int line = 0;
    unsigned int lineStart = 0;
    bool newline = true;

    while (!ls.buf[ls.pos] == '\0')
    {
        //fprintf(stderr, "Read %d (%d.%d) : %c\n", ls.pos, line, ls.pos-lineStart, ls.buf[ls.pos]);
        unsigned int start = ls.pos - lineStart;
        if (newline) {
            unsigned int newDepth = getDepth(&ls);
            newline = false;

            if (newDepth != ls.depth) {
                unsigned int end = ls.pos - lineStart;
                TokenType tt = (newDepth > ls.depth) ? TOKEN_DEPTH_INCREASE : TOKEN_DEPTH_DECREASE;
                ls.depth = newDepth;
                pushToken(&tk, buildToken(tt, ls.pos, line, start, end));
                continue;
            }
        }

        switch (ls.buf[ls.pos])
        {
            case '#':
                ls.pos++;
                while(ls.buf[ls.pos] != '\n' && ls.buf[ls.pos] != '\0') ls.pos++;
                break;
            case ' ':
                ls.pos++;
                break;
            case '\n':
                newline = true;
                pushToken(&tk, buildToken(TOKEN_NEWLINE, ls.pos, line, start, start+1));
                line++;
                ls.pos++;
                lineStart = ls.pos;
                break;
            case '/':
                if (ls.buf[ls.pos+1] == '/') {
                    pushToken(&tk, buildToken(TOKEN_DIVIDE, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_TRUEDIVIDE, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '=':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_EQUALS, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_ASSIGN, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '<':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_LESS_EQUAL, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_LESS, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '!':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_DIFF, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_BANG, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '>':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_GREATER_EQUAL, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_GREATER, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '*':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_AUG_TIMES, ls.pos, line, start, start + 2));
                    ls.pos += 2;
                } else if (ls.buf[ls.pos+1] == '*') {
                    pushToken(&tk, buildToken(TOKEN_POWER, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_TIMES, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '-':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_AUG_MINUS, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_MINUS, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '+':
                if (ls.buf[ls.pos+1] == '=') {
                    pushToken(&tk, buildToken(TOKEN_AUG_PLUS, ls.pos, line, start, start+2));
                    ls.pos += 2;
                } else {
                    pushToken(&tk, buildToken(TOKEN_PLUS, ls.pos, line, start, start+1));
                    ls.pos++;
                }
                break;
            case '(': pushToken(&tk, buildToken(TOKEN_LPAREN, ls.pos, line, start, start+1)); ls.pos++; break;
            case ')': pushToken(&tk, buildToken(TOKEN_RPAREN, ls.pos, line, start, start+1)); ls.pos++; break;
            case '[': pushToken(&tk, buildToken(TOKEN_LBRACKET, ls.pos, line, start, start+1)); ls.pos++; break;
            case ']': pushToken(&tk, buildToken(TOKEN_RBRACKET, ls.pos, line, start, start+1)); ls.pos++; break;
            case ':': pushToken(&tk, buildToken(TOKEN_COLON, ls.pos, line, start, start+1)); ls.pos++; break;
            case ';': pushToken(&tk, buildToken(TOKEN_SEMICOLON, ls.pos, line, start, start+1)); ls.pos++; break;
            case ',': pushToken(&tk, buildToken(TOKEN_COMMA, ls.pos, line, start, start+1)); ls.pos++; break;
            case '%': pushToken(&tk, buildToken(TOKEN_MODULO, ls.pos, line, start, start+1)); ls.pos++; break;
            case '"':
            case '\'': {
                // TODO utf-8 strings
                unsigned int length = 0;
                unsigned int i = 0;
                char quote = ls.buf[ls.pos];
                ls.pos++;

                while (ls.buf[ls.pos+i] != '\0') {
                    if (ls.buf[ls.pos+i] == quote)
                        break;
                    if (ls.buf[ls.pos+i] == '\\') {
                        i++;
                    }
                    i++;
                    length++;
                }

                char *text = (char *)malloc(sizeof(char) * (length+1));
                unsigned int j = 0;
                i = 0;

                while (ls.buf[ls.pos+i] != '\0') {
                    if (ls.buf[ls.pos+i] == quote)
                        break;
                    if (ls.buf[ls.pos+i] == '\\') {
                        i++;
                    }
                    text[j] = ls.buf[ls.pos+i];
                    i++;
                    j++;
                }

                text[length] = '\0';

                pushToken(&tk, buildTokenString(ls.pos-1, line, start, start+length+1, text));

                ls.pos += length + 1;

                break;}
            default:
                if (isalpha(ls.buf[ls.pos]))
                {
                    int n = 1;
                    while (ls.buf[ls.pos+n] != '\0' && isident(ls.buf[ls.pos+n])) n++;
                    char *buf = strndup(ls.buf+ls.pos, n);
                    if (isKeyword(buf)) {
                        pushToken(&tk, buildTokenKeyword(ls.pos, line, start, start+n,
                                convertToKeyword(buf)));
                        free(buf);
                    } else
                        pushToken(&tk, buildTokenIdentifier(ls.pos, line, start, start+n, buf));
                    ls.pos += n;
                    break;
                }
                if (ls.buf[ls.pos] == '.' && ls.buf[ls.pos+1] != '\0' && !isdigit(ls.buf[ls.pos+1]))
                {
                    pushToken(&tk, buildToken(TOKEN_COMMA, ls.pos, line, start, start+1)); ls.pos++; break;
                }
                if (ls.buf[ls.pos] == '-' && ls.buf[ls.pos+1] != '\0' && !isdigit(ls.buf[ls.pos+1]))
                {
                    pushToken(&tk, buildToken(TOKEN_MINUS, ls.pos, line, start, start+1)); ls.pos++; break;
                }

                if (ls.buf[ls.pos] == '.' || isdigit(ls.buf[ls.pos]) || ls.buf[ls.pos] == '-')
                {
                    bool minus = ls.buf[ls.pos] == '-';
                    if (minus) ls.pos += 1;
                    bool isFloat = false;
                    int posFloat = -1;
                    int endInt = -1;
                    int n = 0;
                    while (ls.buf[ls.pos+n] != '\0'
                            && (isdigit(ls.buf[ls.pos+n]) || ls.buf[ls.pos+n] == '.'))
                    {
                        if (ls.buf[ls.pos+n] == '.')
                        {
                            endInt = n;
                            isFloat = true;
                            posFloat = n+1;
                        }
                        n++;
                    }
                    if (!isFloat) endInt = n;

                    int v = 0;
                    for (int i = 0; i < endInt; i++)
                        v = 10 * v + (ls.buf[ls.pos+i]-'0');
                    if (!isFloat) {
                        pushToken(&tk, buildTokenInteger(ls.pos, line, start, start+n,v));
                    } else {
                        double r = 0;
                        for (int i = n-1; i >= posFloat; i--)
                            r = 0.1 * r + (ls.buf[ls.pos+i]-'0');
                        r = v + 0.1 * r;
                        r = minus ? -r : r;
                        pushToken(&tk, buildTokenFloat(ls.pos, line, start, start+n, r));
                    }
                    ls.pos += n;
                    break;
                }

                /* error token */
                ls.pos++;
                break;
        }
    }

    if (tk.tokens[tk.numTokens-1].type != TOKEN_NEWLINE) {
        pushToken(&tk, buildToken(TOKEN_NEWLINE, ls.pos, line+1, 0, 0));
    }

    return tk;
}

const char *sprint_tokenType(TokenType tt)
{
    switch(tt) {
        case TOKEN_DEPTH_INCREASE: return "->";
        case TOKEN_DEPTH_DECREASE: return "<-";
        case TOKEN_NEWLINE: return "\\n";
        case TOKEN_OPERATOR: return "op";
        case TOKEN_IDENT: return "id";
        case TOKEN_KEYWORD: return "key";
        case TOKEN_INTEGER: return "int";
        case TOKEN_FLOAT: return "float";
        case TOKEN_LPAREN: return "(";
        case TOKEN_RPAREN: return ")";
        case TOKEN_LBRACKET: return "[";
        case TOKEN_RBRACKET: return "]";
        case TOKEN_COLON: return ":";
        case TOKEN_COMMA: return ",";
        case TOKEN_DOT: return ".";
        case TOKEN_ASSIGN: return "=";
        case TOKEN_EQUALS: return "==";
        case TOKEN_MINUS: return "-";
        case TOKEN_PLUS: return "+";
        case TOKEN_TIMES: return "*";
        case TOKEN_DIVIDE: return "/";
        case TOKEN_TRUEDIVIDE: return "//";
        case TOKEN_SEMICOLON: return ";";
        case TOKEN_AUG_TIMES: return "*=";
    }

    return "ERR";
}

char *sprint_keyword(Keyword k)
{
    switch(k) {
        case KEYWORD_FOR: return "for";
        case KEYWORD_WHILE: return "while";
        case KEYWORD_IN: return "in";
    }

    return "ERR";
}

void pprint_token(Token t)
{
    printf("{%s", sprint_tokenType(t.type));
    if (t.type == TOKEN_IDENT)
        printf(":%s", t.text);
    if (t.type == TOKEN_KEYWORD)
        printf(":%s", sprint_keyword(t.keyword));
    if (t.type == TOKEN_INTEGER)
        printf(":%d", t.integer);
    if (t.type == TOKEN_FLOAT)
        printf(":%f", t.floating);
    printf("}");
}

void pprint_tokens(Tokens tk)
{
    for (int i = 0; i < tk.numTokens; i++)
        pprint_token(tk.tokens[i]);
    printf("\n");
}
