CMAKE=/home/marc/dev/clion-2018.2.5/bin/cmake/linux/bin/cmake
EMSCRIPTEN_DIR=/home/marc/dev/emsdk/emscripten/1.38.18
EMSCRIPTEN_TOOLCHAIN_FILE=$EMSCRIPTEN_DIR/cmake/Modules/Platform/Emscripten.cmake
mkdir -p cmake-build-emscripten
cd cmake-build-emscripten
cmake .. -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_TOOLCHAIN_FILE=$EMSCRIPTEN_TOOLCHAIN_FILE
make -j4  picopython_interface
cd ..
# personal uploading script to check it online
sh upload.sh
