#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "parser.h"
#include "evaluator.h"

#include <emscripten/emscripten.h>

EMSCRIPTEN_KEEPALIVE
void evaluateString(char *s)
{
    Tokens tk = stringToTokens(s);

    ParserState ps = initParser(s, &tk);
    SyntaxTree *st = parseInput(&ps);

    if (ps.error) {
        printf("%s\n", ps.errorDescription);
        return;
    }

    Environment env = initMainEnvironment();
    evaluate(env, st);
}
