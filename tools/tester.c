#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "parser.h"
#include "evaluator.h"

int main(int argc, char **argv) 
{
    FILE *fp;

    fp = fopen("../examples/main.py", "r");
    fseek(fp, 0, SEEK_END);

    unsigned int n = ftell(fp);

    char *s = (char *)malloc(sizeof(char) * (n+1));

    fseek(fp, 0, SEEK_SET);
    fread(s, 1, n, fp);
    s[n] = '\0';

    Tokens tk = stringToTokens(s);

    ParserState ps = initParser(s, &tk);
    SyntaxTree *st = parseInput(&ps);

    //repr_tree(st);

    if (ps.error) {
        fprintf(stderr, "%s\n", ps.errorDescription);
        free(s);
        return 1;
    }

    //repr_tree(st);

    Environment env = initMainEnvironment();
    evaluate(env, st);
    //pprint_env(env);

    free(s);
    return 0;
}
