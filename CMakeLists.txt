cmake_minimum_required(VERSION 3.10)
project(picopython C)

set(CMAKE_C_STANDARD 11)

add_library(picopython
        src/lexer.c src/parser.c include/lexer.h include/parser.h include/ast.h src/ast.c src/evaluator.c include/evaluator.h src/hashtable.c include/hashtable.h)

include_directories(./include)
target_link_libraries(picopython m)

if (EMSCRIPTEN)
    add_executable(picopython_interface tools/interface.c)
    target_link_libraries(picopython_interface picopython)

    set(EXPORT_FUNCTIONS_FLAGS
            "-s EXPORTED_FUNCTIONS='[\"_evaluateString\"]' \
            -s EXTRA_EXPORTED_RUNTIME_METHODS='[\"ccall\", \"cwrap\"]'")
    set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} \
        ${EXPORT_FUNCTIONS_FLAGS}")
else()
    add_executable(picopython_test tools/tester.c)
    target_link_libraries(picopython_test picopython)
endif()

